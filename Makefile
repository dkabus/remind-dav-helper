APP=remind-dav
PREFIX=/usr/local
SYSTEMD=/usr/lib/systemd/user

all:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f edit 	$(DESTDIR)$(PREFIX)/bin/$(APP)-edit
	cp -f import 	$(DESTDIR)$(PREFIX)/bin/$(APP)-import
	cp -f helper 	$(DESTDIR)$(PREFIX)/bin/$(APP)-helper
	cp -f run 	$(DESTDIR)$(PREFIX)/bin/$(APP)-sync
	cp -f session 	$(DESTDIR)$(PREFIX)/bin/$(APP)-session
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)-edit
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)-import
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)-helper
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)-sync
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)-session
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp -f import.desktop $(DESTDIR)$(PREFIX)/share/applications/$(APP)-import.desktop
	mkdir -p $(DESTDIR)$(SYSTEMD)
	[ -n "$(DESTDIR)$(SYSTEMD)" ] && cp -f service $(DESTDIR)$(SYSTEMD)/remind.service
	[ -n "$(DESTDIR)$(SYSTEMD)" ] && cp -f timer $(DESTDIR)$(SYSTEMD)/remind.timer

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)-edit
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)-import
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)-helper
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)-sync
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)-session
	rm -f $(DESTDIR)$(PREFIX)/share/applications/$(APP)-import.desktop
	[ -n "$(DESTDIR)$(SYSTEMD)" ] && rm -f $(DESTDIR)$(DESTDIR)$(SYSTEMD)/remind.service
	[ -n "$(DESTDIR)$(SYSTEMD)" ] && rm -f $(DESTDIR)$(DESTDIR)$(SYSTEMD)/remind.timer

.PHONY: all install uninstall
