# Desmond Kabus' calendar setup using `remind`
## Usage
This package provides the commands:

- `remind-dav-edit`
- `remind-dav-import`
- `remind-dav-helper`
- `remind-dav-session`
- `remind-dav-sync`

Most importantly: check the manual and/or README
for `remind`, `rem`, `remind-caldav`, and `remind-agenda`.
These scripts mostly use commands of these packages.

For example, `rs` opens a session to edit calendars and
correctly syncs them before and afterwards.
I am also using automatic sync with DAV with `remind-caldav`
and desktop notifications using `notify-send`.

Reminders are only pushed, if local changes are detected.

## Manual installation
Install dependencies, e.g. on [Arch Linux](https://archlinux.org/):
```sh
sudo pacman -S remind python-keyring
pip install --user remind-caldav
```
then `make && sudo make install` my own `remind-agenda` and this repository

## Configuration
Add the to-be-used password to `python-keyring` for sync with DAV:
```sh
keyring set www.example.com johndoe
```
or using `netrc`. (See documentation of `remind-caldav` for details.

Keep `$HOME` clean by pointing `remind` to your reminders file.
For this add this to `.profile` for instance:
```sh
export DOTREMINDERS="$HOME/.config/remind/main.rem"
```
I use multiple files, one for each calendar, that are included in main.rem.

In the same folder, in the example `~/.config/remind`, create the file `dav`.
This file could contain these commands to sync your calendars with a [Nextcloud](https://www.nextcloud.com) instance:
```sh
export DAVREMOTE=https://nextcloud.example.com/remote.php/dav/calendars/desmond
export DAVUSER=desmond
remind-dav-helper pull "work" "work.rem"
remind-dav-helper pullbd "contact_birthdays" "birthdays.rem"
remind-dav-helper push "holidays" "holidays.rem"
```

For notifications, add to autostart, e.g. in `.xprofile`:
```sh
rem -z -k"notify-send -u critical 'Reminder' '%s'&" & # deamon
```

If you are on a distribution that uses [`systemd` like Arch Linux](https://wiki.archlinux.org/index.php/Systemd/),
you might want to turn on the timer for automatic sync with DAV:
```sh
systemctl enable --user --now remind.timer
```
Otherwise just run the `remind-dav-sync` script whenever you want to sync.
